#!/usr/bin/env bash
set -e
SCRIPTPATH=$(readlink -f "$0")
THISDIR=$(dirname "$SCRIPTPATH")

KERNEL_VERSION=$(ls build/vmlinu* | cut -c 15-)
if [[ $(id -u) -ne 0 ]]; then
	echo "Please run this script with superuser privledges"
	exit
fi

if [[ ! -d build ]]; then
	echo "The build directory does not exist, please place a kernel in it"
	exit
fi

if [ ! -x "$(command -v pwgen)" ] && [ -z ${ROOT_PASSWORD+x} ]; then
	echo -n "pwgen is needed to generate a random root password, unless one is supplied with "
	echo "ROOT_PASSWORD"
	exit
fi

ROOT_PASSWORD=${ROOT_PASSWORD:-`pwgen 16 1`}

echo "Building with kernel version $KERNEL_VERSION"

sleep 5

modprobe nbd max_part=8
qemu-img create -f qcow2 rockstor.qcow2 7G
qemu-nbd -c /dev/nbd0 rockstor.qcow2
parted --script /dev/nbd0 \
    mklabel gpt \
    mkpart efi fat32 0% 140MiB \
    mkpart boot ext4 140MiB 256MiB \
    mkpart system ext4 256MiB 100% \
    set 1 boot on \
    set 1 esp on 

partx -a /dev/nbd0 || echo "partx failed - you can ignore this"
partprobe /dev/nbd0
mkfs.fat -F 32 /dev/nbd0p1 

mkfs.ext4 /dev/nbd0p2 
mkfs.btrfs /dev/nbd0p3
mkdir -p /mnt/fedora/
mount /dev/nbd0p3 /mnt/fedora/
btrfs subvolume create /mnt/fedora/root/
umount /mnt/fedora
mount -t btrfs -o subvol=root /dev/nbd0p3 /mnt/fedora
btrfs filesystem label /mnt/fedora rockstor_rockstor
mkdir -p /mnt/fedora/boot/

mount /dev/nbd0p2 /mnt/fedora/boot/
mkdir -p /mnt/fedora/boot/efi/
mount /dev/nbd0p1 /mnt/fedora/boot/efi/
sleep 5

yum -y --releasever=7 --installroot=/mnt/fedora groupinstall core
mount --bind /dev /mnt/fedora/dev/
mount --bind /sys /mnt/fedora/sys/
mount -t proc none /mnt/fedora/proc/
cp /etc/resolv.conf /mnt/fedora/etc/

chroot /mnt/fedora /bin/sh -c "yum -y --releasever=7 install centos-release"
chroot /mnt/fedora /bin/sh -c "yum group install 'Minimal Install'"

# Install rockstor deps
chroot /mnt/fedora /bin/sh -c "yum -y install epel-release"
chroot /mnt/fedora /bin/sh -c "yum -y install zeromq zeromq-devel python-devel postgresql-libs postgresql-devel openssl postgresql-server python-zmq python-psycopg2 nginx samba btrfs-progs cryptsetup ntp"
chroot /mnt/fedora /bin/sh -c "yum -y install shellinabox smartmontools usbutils nut nut-xml postfix nano net-snmp rsync"

# Rockstor Development deps for conveinience
chroot /mnt/fedora /bin/sh -c "yum -y groupinstall \"Development Tools\" \"Development Libraries\""
chroot /mnt/fedora /bin/sh -c "yum -y install git"

chroot /mnt/fedora /bin/sh -c "cd /opt/; git clone https://github.com/rockstor/rockstor-core.git"
cat rockstor-config-changes.patch | sed "s/kernel_version_here/${KERNEL_VERSION}/g" > /mnt/fedora/opt/rockstor-core/rockstor-config-changes.patch
chroot /mnt/fedora /bin/sh -c "cd /opt/rockstor-core/; git am rockstor-config-changes.patch"

cp -r rockstor-patches /mnt/fedora/opt/rockstor-core/
chroot /mnt/fedora /bin/sh -c "cd /opt/rockstor-core/; git am rockstor-patches/*.patch"

chroot /mnt/fedora /bin/sh -c "cd /opt/rockstor-core/; python bootstrap.py -c prod-buildout.cfg"
chroot /mnt/fedora /bin/sh -c "cd /opt/rockstor-core/; ./bin/buildout"

# Copy the kernel
cp build/vmlinu* /mnt/fedora/boot/ || :
cp build/System.map* /mnt/fedora/boot/
cp build/config* /mnt/fedora/boot/ || :
mkdir -p /mnt/fedora/lib/modules/
cp -r build/mods/lib/modules/* /mnt/fedora/lib/modules/
ls -la /mnt/fedora/lib/modules/


chroot /mnt/fedora /bin/sh -c "yum -y install grub2"

#chroot /mnt/fedora /bin/sh -c 'grub-install --efi-directory=/boot --no-nvram --removable'

if [ ! -d /mnt/fedora/etc/default/ ]; then
	mkdir -p /mnt/fedora/etc/default/
fi

ROOT_DEVICE=`blkid /dev/nbd0p3 -o value -s PARTUUID`
cat <<EOF > /mnt/fedora/etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="console=ttyS0,115200 rootwait net.ifnames=0"
GRUB_DEVICE_UUID=${ROOT_DEVICE}
GRUB_TERMINAL="serial"
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1"
EOF

#chroot /mnt/fedora /bin/sh -c 'update-grub'
chroot /mnt/fedora /bin/sh -c "grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg"

# As grub doesn't want to generate with a UUID (even when specified, force it)
sed -i "s/root=\/dev\/nbd0p3/root=PARTUUID=${ROOT_DEVICE} rootflags=subvol=root/g" /mnt/fedora/boot/efi/EFI/centos/grub.cfg
grep "PARTUUID=${ROOT_DEVICE}" /mnt/fedora/boot/efi/EFI/centos/grub.cfg
if [[ "$?" -ne 0 ]]; then
	echo "ERROR: PARTUUID is not in the generated grub.cfg, the script may need to be adjusted"
	echo "NOTE: /mnt/fedora is still mounted for debugging purposes"
	exit
fi

# Fedora installs its GRUB EFI into EFI/fedora/grubaa64.efi, and on a real system,
# expects EFI variables to point to it. Here, we need to act like we are on removable media
mkdir /mnt/fedora/boot/efi/EFI/boot/
cp /mnt/fedora/boot/efi/EFI/centos/grubaa64.efi /mnt/fedora/boot/efi/EFI/boot/bootaa64.efi
echo "rockstor-preconfig" > /mnt/fedora/etc/hostname

#cat <<EOF > /mnt/fedora/etc/network/interfaces.d/eth0
#auto eth0
#iface eth0 inet dhcp
#EOF

chroot /mnt/fedora/ /bin/sh -c "echo root:$ROOT_PASSWORD | chpasswd; echo \"*** root password is $ROOT_PASSWORD ***\""
chroot /mnt/fedora /bin/sh -c "depmod -a `ls /lib/modules/`" || : # depmod complains about the running kernel not being there

BOOT_DEVICE=`blkid /dev/nbd0p2 -o value -s PARTUUID`
EFI_DEVICE=`blkid /dev/nbd0p1 -o value -s PARTUUID`
cat <<EOF > /mnt/fedora/etc/fstab
PARTUUID=${ROOT_DEVICE}		/		btrfs	subvol=root,noatime	1	1
PARTUUID=${BOOT_DEVICE}		/boot	ext4	defaults	1	1
PARTUUID=${EFI_DEVICE}		/boot/efi vfat defaults	1	1
EOF

# gpg-agent seems to hang around, kill it
chroot /mnt/fedora /bin/sh -c "killall gpg-agent" || :

# Make sure we boot to console
chroot /mnt/fedora /bin/sh -c "systemctl set-default multi-user.target"

# Make sure /var/run is symlinked correctly, or the systemd-dbus does not work
chroot /mnt/fedora /bin/sh -c "rm -rf /var/run; cd /var/; ln -s /run run"

# Disable irqbalance - causes issues with DPAA Ethernet on LS1043
chroot /mnt/fedora /bin/sh -c "systemctl disable irqbalance"

# Enable rockstor
chroot /mnt/fedora /bin/sh -c "systemctl enable rockstor-pre"

# Teardown
umount -R /mnt/fedora
qemu-nbd -d /dev/nbd0
echo $?
if [[ "$?" -eq 0 ]]; then
       qemu-nbd -d /dev/nbd0
fi
