#!/bin/sh
# Example script to launch a VM using the generated Debian image
sudo qemu-system-aarch64 -enable-kvm -m 1100 -cpu host -M virt,gic_version=2 -smp 1 -nographic -bios "QEMU_EFI.fd" \
	-drive if=none,file=debian.qcow2,id=hd0  -serial tcp::4446,server,telnet -monitor stdio \
	-device virtio-blk-device,drive=hd0 \
	-netdev type=user,id=net0 -device virtio-net-pci,netdev=net0
